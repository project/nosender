CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

No Sender suppresses the inclusion of a Sender header in email generated from
the site. The Mime Mail module also happens to do what this module does, amidst
plenty of other features. Sites without one of these modules (or custom code to
alter the behavior) will send email with the Sender header set to the site admin
email address (the "Site Details" > "E-mail address" field at
admin/config/system/site-information) even for messages where the From header is
set otherwise, for example via Webform configuration.

Mail clients render the Sender header on received mail in various ways,
generally very near to the From header. Some will actually label them discretely
("From: [from address]" / "Sender: [sender address]") while others will combine
the two addresses into a single phrase ("[from address] sent by [sender
address]" or "[sender address] on behalf of [from address]" or "[from address]
via [sender address]"). Furthermore, the "Block Sender" feature in some mail
clients will actually add the sender address to the user's blacklist, which can
mean that all mail from your site gets blacklisted when perhaps the user only
intended to block the from address (a subset of mail from your site).

 * For a full description of the module, visit the project page:
   https://drupal.org/project/nosender

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/nosender


REQUIREMENTS
------------

No special requirements


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the Sender header from being included in any
email messages created with the drupal_mail() function, unless another module
with more weight also implements hook_mail_alter() and sets the Sender header.
To get the Sender header back, disable the module.
